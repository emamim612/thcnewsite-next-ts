import 'src/styles/app.css'
import type { AppProps } from 'next/app'
import "swiper/swiper-bundle.min.css";
import "swiper/swiper.min.css";

import "src/assets/css/HeroParticlesVideo.css";
import "src/assets/css/layout.css";

import 'reactjs-popup/dist/index.css';
import "src/assets/css/modal.css";

import '../assets/css/home.css';

import "../assets/roadmap/roadmap.css";

import ContextProvider, { useBlockchainContext } from "src/contexts";

import { WalletProvider } from '@txnlab/use-wallet'

export default function App({ Component, pageProps }: AppProps) {
  return <ContextProvider><Component {...pageProps} /></ContextProvider>
}
