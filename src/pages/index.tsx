import Home from 'src/component/home'

import Head from 'next/head';

const index = () => {
  return (
    <>
      <Head>
        <title>Transhuman Coin</title>
        <link rel="icon" href="assets/logo.png" />
      </Head>
      <Home />
    </>
  )
}

export default index