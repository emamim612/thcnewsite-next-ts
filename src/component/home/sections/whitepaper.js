const Whitepaper = () => {
    return (
        <section className="site-black-color z-10 overflow-hidden relative svelte-am2ffe lazyloaded" style={{ paddingTop: '0px' }}>
            <div className="site-black-color" style={{ paddingBottom: '100px', background: '#e4eaf8' }}>
                <div className="container site-black-color" style={{ color: 'rgb(19, 31, 55)' }}>
                    <div className="site-black-color mx-auto svelte-am2ffe pt7" style={{ color: 'rgb(19, 31, 55)' }}>
                        <div className="site-black-color row justify-between svelte-1xwyt96" style={{ color: 'rgb(19, 31, 55)' }}>
                            <div className="site-black-color lg:col-6 svelte-1be2l8v" style={{ color: 'rgb(19, 31, 55)' }}>
                                <div className="mb-8">
                                    <div className="eyebrow text-green-regular  svelte-11fnwb7">AUDITED-SAFE COIN</div>
                                </div>
                                <div className="title site-black-color" style={{ color: 'rgb(19, 31, 55)' }}>
                                    <div className="mb4">
                                        <div style={{ fontSize: '2.5rem', fontWeight: '700', color: 'rgb(19, 31, 55)' }}>The Ideology</div>
                                    </div>
                                </div>
                                <div className="site-black-color" style={{ fontSize: '1rem', color: 'rgb(19, 31, 55)' }}>
                                    <div className="" style={{ fontWeight: 700 }}>
                                        <div className="site-black-color mt1" style={{ color: 'rgb(19, 31, 55)' }}>
                                            What is The Fire Under Transhuman Coin?
                                            Transhumanism is a philosophical movement devoted to promoting the research and development of robust human-enhancement technologies. Such technologies would augment or increase human sensory reception, emotive ability, or cognitive capacity as well as radically improve human health and extend human life spans.
                                        </div>
                                        <div className="site-black-color mt1" style={{ color: 'rgb(19, 31, 55)' }}>
                                            The Transhuman Coin is a cryptocurrency dedicated to funding researches and development of technologies that enhance Human Life Experience. We will donate to causes that promote human life. In 1 year after launching, we hope to report on a successful project funded by the Transhuman Coin.
                                        </div>
                                        <div className="site-black-color mt1" style={{ color: 'rgb(19, 31, 55)' }}>
                                            There have been a lot of RFI type projects lately but none of them had an actual utility that would make the project useful longterm. THC is the coin that invests in Future Human Technologies. We are futuristic. Our solution is to make a token that will not only give holders 2% reward on every transaction, but also send 2% of every transaction to the Transhuman Charity Wallet while manually making regular donations to the Binance charity donation wallet.
                                        </div>
                                        <div className="site-black-color mt1" style={{ color: 'rgb(19, 31, 55)' }}>
                                            We will ensure transparency and accountability by ensuring that all donations are made to reputable research centers which will be made public while employing reputable auditors to check the books.
                                        </div>
                                    </div>
                                </div>
                                <div className="justify mt1">
                                    <div className="cta mt0i mr1" >
                                        <a target="_blank" className="btn inline-block cursor-pointer green svelte-lt6zv4" href="/assets/pdf/thcaudit.pdf">
                                            <div className="py-4 px-6 grow">AUDITED BY TECHRATE</div>
                                        </a>
                                    </div>
                                    <div className="cta mt0i">
                                        <a target="_blank" className="btn inline-block cursor-pointer green svelte-lt6zv4" href="/assets/pdf/whitepaperthc.pdf">
                                            <div className="py-4 px-6 grow">GET WHITEPAPER</div>
                                        </a>
                                    </div>
                                </div>

                            </div>
                            <div className="media fade-in-bottom w-full md:tablet-col-5 lg:col-5 svelte-1ntb9k9 fade-in-bottom-animate">
                                <div className="img-wrapper flex items-baseline svelte-1ntb9k9 fd-c">
                                    <img className="w-auto z-10 relative svelte-1ntb9k9 lazyloaded border-top10" data-src="" alt="whitepaper" src="/assets/Page-14-1.jpg" />
                                    <img className="w-auto z-10 relative svelte-1ntb9k9 lazyloaded " data-src="" alt="whitepaper" src="/assets/Page-14-2.jpg" />
                                    <img className="w-auto z-10 relative svelte-1ntb9k9 lazyloaded border-bottom10" data-src="" alt="whitepaper" src="/assets/whitepaper.jpg" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section >
    )
}
export default Whitepaper;